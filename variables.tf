variable domain {
  type        = string
  description = "The domain name to manage."
  default     = "src.eco"
}

variable gateway {
  type        = string
  description = "The target of the core domain."
  default     = "the-resistance.gitlab.io"
}

variable gateway_type {
  type        = string
  description = "The type of gateway record to create."
  default     = "CNAME"
}

variable verifications {
  type        = map
  description = "The TXT record key used for verification of the subdomain."
  default     = {
    "chatbook.dev" = {
      "_gitlab-pages-verification-code.chatbook.dev" = "gitlab-pages-verification-code=5221795b7b7a884b8e156229e0f618aa"
    }
    "chatbook.org" = {
      "_gitlab-pages-verification-code.chatbook.org" = "gitlab-pages-verification-code=e9204a8c4a1371d9d11b2290a3732d16"
    }
    "src.eco" = {
      "_gitlab-pages-verification-code.src.eco" = "gitlab-pages-verification-code=b555c98b0cd18717f375aa7743460a30"
      "_dnslink"                                = "dnslink=/ipfs/bafybeigruephqu552yew5iio7u4pc4ozu2lnir5xskdaggvhcurxlhlchi"
    }
  }
}
