# src.eco

The artificial intelligence that tuned a universe.

## Getting started

1. Install Node.js via [the appropriate method here](https://nodejs.org/en/download/).
2. [Clone this repository](https://gitlab.com/the-resistance/src.eco) to your computer.
3. Use the provided convenience tasks for Node.js (in package.json) and VSCode (in .vscode/tasks.json or at 'Ctrl + Shift + P > Tasks: Run Task').

## Run this project in your browser

1. Navigate to this project in your terminal.
2. Run task: `npm start`
3. This site will be available at: https://localhost:1234
