import { seededPRNG } from './common'

/** Constructor for the NameGenerator class */
const NameGenerator = {
    /** given a list of strings, add each to the corpus of the model */
    initialize: function (this: any, corpus: [string]) {
        this.transitions = [[]]
        this.corpus = []
        corpus.forEach(
            function (this: any, string: string) {
                this.integrateString(string)
            }.bind(this)
        )
        return this
    },
    /** select one from an array, randomly */
    selectOne: function (a: [string], seed: number) {
        return a[Math.floor(seed * a.length)]
    },
    /** execute f for each character in s */
    forEachCharacter: function (s: string, f: any) {
        Array.prototype.forEach.call(s, f)
    },
    /** add an element to the transition table for lastLetter.  If such a
     * sub-table doesn't exist, create it.*/
    addTableElement: function (
        table: any,
        lastLetter: string,
        currentLetter: string
    ) {
        const array = table[lastLetter] || []
        array.push(currentLetter)
        table[lastLetter] = array
    },
    /** integrate a string into the transition model */
    integrateString: function (this: any, string: string) {
        let strLen = string.length,
            initial = this.transitions[0],
            lastLetter = string[0],
            currentLetter,
            currentTable,
            i
        if (strLen > 0) {
            initial.push(string[0])
            this.forEachCharacter(
                string.substring(1),
                function (this: any, currentLetter: string, index: number) {
                    this.addTableElement(
                        this.getNthTable(index + 1),
                        lastLetter,
                        currentLetter
                    )
                    lastLetter = currentLetter
                }.bind(this)
            )
            this.addTableElement(this.getNthTable(strLen), lastLetter, 'end')
            this.corpus.push(string)
        }
        return this
    },
    /** get the Nth transition table for this model, or create it if it doesn't yet exist. */
    getNthTable: function (this: any, n: number) {
        const table = this.transitions[n] || {}
        this.transitions[n] = table
        return table
    },
    /** generate one random name */
    generateOne: function (this: any, seedString: string) {
        let seed = seededPRNG(seedString)
        let name = [this.selectOne(this.transitions[0], seed)],
            lastLetter = name[0],
            done = false,
            i = 1,
            currentLetter

        while (!done) {
            seedString = seedString + 'a'
            seed = seededPRNG(seedString)
            currentLetter = this.selectOne(
                this.getNthTable(i)[lastLetter],
                seed
            )
            if (currentLetter === 'end') {
                done = true
            } else {
                name.push(currentLetter)
                lastLetter = currentLetter
                i = i + 1
            }
        }
        return name.join('')
    }
}

const corpus: any[string] = [
    'Abbadon',
    'Abraxas',
    'Adramalech',
    'Agrith-Naar',
    'Ahpuch',
    'Ahriman',
    'Aku',
    'Alastair',
    'Alastor',
    'Algaliarept',
    'Alichino',
    'Amaimon',
    'Amon',
    'Andariel',
    'Andromeda',
    'Angel',
    'Antlia',
    'Anyanka',
    'Anzu',
    'Apollyon',
    'Apus',
    'Aquarius',
    'Aquila',
    'Ara',
    'Archimonde',
    'Aries',
    'Artery',
    'Asmodeus',
    'Astaroth',
    'Asura',
    'Auriga',
    'Azal',
    'Azazeal',
    'Azazel',
    'Azmodan',
    'Azura',
    'Baal',
    'Baalberith',
    'Babau',
    'Bacarra',
    "Bal'lak",
    'Balaam',
    'Balor',
    'Balrog',
    'Balthazar',
    'Baphomet',
    'Barakiel',
    'Barbariccia',
    'Barbas',
    'Bartimaeus',
    'Bast',
    "Bat'Zul",
    "Be'lakor",
    'Beastie',
    'Bebilith',
    'Beelzebub',
    'Behemoth',
    'Beherit',
    'Beleth',
    'Belfagor',
    'Belial',
    'Belphegor',
    'Belthazor',
    'Berry',
    'Betelguese',
    'Bile',
    'Blackheart',
    'Boötes',
    'Cacodemon',
    'Cadaver',
    'Caelum',
    'Cagnazzo',
    'Calcabrina',
    'Calcifer',
    'Camelopardalis',
    'Cancer',
    'Canes Venatici',
    'Canis Major',
    'Canis Minor',
    'Capricornus',
    'Carina',
    'Cassiopeia',
    'Castor',
    'Centaurus',
    'Cepheus',
    'Cetus',
    'Chamaeleon',
    'Chemosh',
    'Chernabog',
    'Cherry',
    'Cimeries',
    'Circinus',
    'Ciriatto',
    'Claude',
    'Columba',
    'Coma Berenices',
    'Cordelia',
    'Corona Austrina',
    'Corona Borealis',
    'Corvus',
    'Coyote',
    'Crater',
    'Crawly',
    'Crowley',
    'Crux',
    'Cryto',
    'Cyberdemon',
    'Cygnus',
    "D'Hoffryn",
    'Dabura',
    'Dagon',
    'Damballa',
    'Dante',
    'Darkseid',
    'Decarbia',
    'Delphinus',
    'Delrith',
    'Demogorgon',
    'Demonita',
    'Devi',
    'Diablo',
    'Diabolus',
    'Dorado',
    'Doviculus',
    'Doyle',
    'Draco',
    'Dracula',
    'Draghinazzo',
    'Dretch',
    'Dumain',
    'Duriel',
    'Emma',
    'Equuleus',
    'Eridanus',
    'Errtu',
    'Etna',
    'Etrigan',
    'Euronymous',
    'Faquarl',
    'Farfarello',
    'Femur',
    'Fenriz',
    'Firebrand',
    'Fornax',
    'Freddy',
    'Furfur',
    'Gaap',
    'Gary',
    'Gemini',
    'Glabrezu',
    'Gorgo',
    'Gothmog',
    'Gregor',
    'Grus',
    'Haborym',
    'Hades',
    'Halfrek',
    "Har'lakk",
    'Hastur',
    'Hecate',
    'Hell',
    'Hellboy',
    'Hercules',
    'Hex',
    'Hezrou',
    'Hiei',
    'Him',
    'Hnikarr',
    'Horologium',
    'Hot',
    'Hydra',
    'Hydrus',
    'Illa',
    'Indus',
    'Infernal',
    'Inferno',
    'Ishtar',
    'Jabor',
    'Jadis',
    'Janemba',
    'Japhrimel',
    'Jennifer',
    'Jormungandr',
    'Juiblex',
    "K'ril",
    "Kal'Ger",
    'Kali',
    'Khorne',
    "Kil'jaeden",
    'Kneesocks',
    'Koakuma',
    'Korrok',
    'Kronos',
    'Lacerta',
    'Laharl',
    'Lamia',
    'Leo Minor',
    'Leo',
    'Lepus',
    'Leviathan',
    'Libicocco',
    'Libra',
    'Ligur',
    'Lilith',
    'Little',
    'Loki',
    'Longhorn',
    'Lorne',
    'Lucifer',
    'Lupus',
    'Lynx',
    'Lyra',
    "Mal'Ganis",
    'Malacoda',
    'Maledict',
    'Malfegor',
    'Malice',
    'Mammon',
    'Mancubus',
    'Mania',
    'Mannoroth',
    'Mantus',
    'Marduk',
    'Marilith',
    'Masselin',
    'Mastema',
    'Meg',
    'Mehrunes',
    'Melek',
    'Melkor',
    'Mensa',
    'Mephisto',
    'Mephistopheles',
    'Metztli',
    'Microscopium',
    'Mictian',
    'Milcom',
    'Moloch',
    'Monoceros',
    'Mormo',
    'Musca',
    "N'zall",
    'Naamah',
    'Nadia',
    'Nalfeshnee',
    'Nanatoo',
    'Nergal',
    'Nero',
    'Neuro',
    'Newt',
    'Nihasa',
    'Nija',
    'Norma',
    'Nosferatu',
    'Nouda',
    'Nurgle',
    'Octans',
    'Ophiuchus',
    'Orion',
    'Ouroboros',
    'Overlord',
    'Oyashiro',
    'Pavo',
    'Pazuzu',
    'Pegasus',
    'Pennywise',
    'Persephone',
    'Perseus',
    'Phoenix',
    'Pictor',
    'Pisces',
    'Piscis Austrinus',
    'Psaro',
    'Puppis',
    'Pwcca',
    'Pwyll',
    'Pyxis',
    'Quasit',
    'Queezle',
    'Qwan',
    'Qweffor',
    'Ra',
    'Rakdos',
    'Ramuthra',
    'Randall',
    'Red',
    'Reticulum',
    'Retriever',
    'Rimmon',
    'Rin',
    'Ronove',
    'Rosier',
    'Rubicante',
    'Ruby',
    'Sabazios',
    'Sagitta',
    'Sagittarius',
    'Samael',
    'Samnu',
    'Satan',
    'Satanas',
    'Sauron',
    'Scanty',
    'Scarlet',
    'Scarmiglione',
    'Scorpius',
    'Sculptor',
    'Scumspawn',
    'Scutum',
    'Sebastian',
    'Sedit',
    'Sekhmet',
    'Sephiroth',
    'Serpens',
    'Set',
    'Sextans',
    'Shaitan',
    'Shax',
    'Shiva',
    'Silitha',
    'Slaanesh',
    'Sparda',
    'Spawn',
    'Spike',
    'Spine',
    'Straga',
    'Supay',
    "T'an-mo",
    'Taurus',
    'Taus',
    'Tchort',
    'Telescopium',
    'Tempus',
    'Tezcatlipoca',
    'Thammaron',
    'Thamuz',
    'Thoth',
    'Tiamat',
    "To'Kash",
    'Toby',
    'Triangulum Australe',
    'Triangulum',
    'Trigon',
    'Tucana',
    'Tunrida',
    'Turok-Han',
    'Typhon',
    'Tzeentch',
    'Ungoliant',
    'Ursa Major',
    'Ursa Minor',
    'Vein',
    'Vela',
    'Vergil',
    'Violator',
    'Virgo',
    'Volans',
    'Vrock',
    'Vulgrim',
    'Vulpecula',
    'Vyers',
    'Ware',
    'Wormwood',
    'Yaksha',
    'Yama',
    'Yaotzin',
    'Yen',
    "Yk'Lagor",
    'Zankou',
    'Zepar',
    'Zuul'
]

export default NameGenerator.initialize(corpus)
