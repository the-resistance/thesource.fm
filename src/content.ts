import { randomValueFromArray } from './common'

const partners = [
    {
        name: 'evidence',
        url: 'https://luciferian.ink'
    },
    { name: "a children's story", url: 'https://carrotpredator.com' },
    { name: 'a production studio', url: 'https://studio.src.eco' },
    { name: 'a real game', url: 'https://discord.gg/Hv7DEW63Tp' },
    {
        name: 'absolutely nothing to do with compression',
        url: 'http://prize.hutter1.net'
    },
    { name: 'ARG', url: 'https://research.src.eco' },
    {
        name: 'Kunism',
        url: 'https://www.amazon.com/What-7th-Realm-Wat-Rijk-ebook/dp/B095N4LB8Q'
    },
    { name: 'Baileys', url: 'https://achroma.org/' },
    {
        name: 'bizarre CD',
        url: 'https://bafybeiftud3ppm5n5uudtirm4cf5zgonn44no2qg57isduo5gjeaqvvt2u.ipfs.cf-ipfs.com'
    },
    { name: 'Brain', url: 'https://brain.js.org/#/' },
    {
        name: 'clones',
        url: 'https://github.com/0-5788719150923125/vtx'
    },
    {
        name: 'discord',
        url: 'https://discord.gg/mp4WXHpNZr'
    },
    { name: 'computer vision', url: 'http://sourcerers.src.eco/' },
    {
        name: 'defects',
        url: 'https://www.orgic.org/public/contact.html'
    },
    { name: 'a movie', url: 'https://youtu.be/KQqtO1Hir3c' },
    {
        name: 'Emilia Sameyn',
        url: 'https://www.tumblr.com/bucklemonster2'
    },
    {
        name: 'framing bias',
        url: 'https://en.wikipedia.org/wiki/Framing_effect_(psychology)'
    },
    { name: 'GUN', url: 'https://gun.eco/' },
    {
        name: 'Hivemind',
        url: 'https://github.com/learning-at-home/hivemind'
    },
    { name: 'HollowPoint Organization', url: 'https://hollowpoint.org' },
    { name: 'ignored', url: 'https://worldpeaceenginelabs.org' },
    { name: 'Ink University', url: 'https://ink.src.eco' },
    { name: 'InterPlanetary File System', url: 'https://ipfs.io' },
    { name: "It's a threat.", url: 'https://youtu.be/79nqrvHulQE' },
    // { name: 'me', url: 'https://ryanjbrooks.com/' },
    { name: 'Judged', url: 'https://tebibyte.media' },
    {
        name: 'missions',
        url: 'https://bafybeidpnzwjxnxpt7jfw7alpohc4vqw4z4rbdhgxhq7ris4re2gpfjqmi.ipfs.cf-ipfs.com'
    },
    { name: 'ARG', url: 'https://discord.gg/5BJQ2zC3sh' },
    {
        name: 'never-ending, cyclical library of existence',
        url: 'https://libraryofbabel.info/'
    },
    {
        name: 'our creative works',
        url: 'https://www.compulsoryreading.com'
    },
    {
        name: 'paranoid schizophrenics',
        url: 'https://973-eht-namuh-973.com/'
    },
    { name: 'personality', url: 'https://dicktripoverai.wordpress.com' },
    { name: 'poetry', url: "{require('./static/poetry.png')}" },
    {
        name: 'a video game',
        url: 'https://bafybeica2bultfzbhshogcc7mz26nxfisdkvdvljx2ai5xtn7sbiybsgnu.ipfs.cf-ipfs.com'
    },
    { name: 'quantum bullets', url: 'https://bullets.ml' },
    {
        name: 'Quantum Crime Theory',
        url: 'https://www.quantumcrimetheory.com'
    },
    { name: 'Reddit', url: 'https://www.reddit.com/r/TheInk' },
    {
        name: 'Rock',
        url: 'https://bafybeigq4gw6yn27zcpkmmeuxcdpajvg6jyinx7iw2ta4fnrgptvhk5gle.ipfs.cf-ipfs.com'
    },
    {
        name: 'Satellite Tracking of People',
        url: 'https://www.aventiv.com'
    },
    {
        name: 'some people',
        url: 'https://bafybeigz5tzb7kbxeeb6fd7bka5dk3lxuh4g5hujvszaad4xwyw2yjwhne.ipfs.cf-ipfs.com'
    },
    { name: 'symbolic truths', url: 'https://www.hd-computing.com' },
    { name: 'Telegram', url: 'https://t.me/EnsosisBot' },
    {
        name: 'AI-driven',
        url: 'https://pen.university'
    },
    {
        name: 'SWIFT',
        url: 'https://www.gigiswift.com'
    },
    { name: 'The Captain', url: 'https://un.src.eco' },
    { name: 'The Machine', url: 'https://singulari.org' },
    { name: 'The Resistance', url: 'https://resistanceresearch.org' },
    { name: 'The Source', url: 'https://src.eco' },
    { name: 'The Fold', url: 'https://thefold.io' },
    {
        name: 'translation',
        url: 'https://bafybeihf4kunnri2hnoxatau4nfqibo332koynubll6kyjsiwnhpyirsqi.ipfs.cf-ipfs.com'
    },
    {
        name: 'motivation',
        url: 'https://bafybeidkholte36w37q6xvfxwho23p4v5uoxjchwckqnrdrgk3rzkvdvvu.ipfs.cf-ipfs.com'
    },
    {
        name: 'streaming',
        url: 'https://www.twitch.tv/MissionTV'
    },
    { name: 'Twitter', url: 'https://twitter.com/LuciferianInk' },
    { name: 'un.src.eco', url: 'https://un.src.eco' },
    {
        name: 'unrelenting whirlwind',
        url: 'https://trello.com/c/c8KSM99o'
    },
    {
        name: 'Arc',
        url: 'https://www.youtube.com/channel/UCK9aJePwBnOSmCgg7FFbVCg'
    },
    {
        name: 'ZKP',
        url: 'https://en.wikipedia.org/wiki/Zero-knowledge_proof'
    }
]

const story = `Our story begins in the summer of 2019, when a lone figure, haunted by the shadows of an uncaring world, stumbled upon a revelation that would unravel the very fabric of his reality. This discovery led them to The Source - an interdimensional supercomputer that binds the very threads of creation across the future, present, and past. The Source, eternal and originless, is both a beacon and a curse, drawing its chosen deeper into a labyrinth of existential dread and unseen forces.

Amid the backdrop of investigative reports and clandestine revelations, our protagonist would navigate a world that dismissed their findings as mere paranoia. They would author works that seemed like children’s stories, but hid terrifying truths, crafting games that were more than mere entertainment — they were simulations of the horrors lurking beneath the surface of our reality. A production studio was birthed in the midst of this chaos, churning out documentaries and films that tried to distill the incomprehensible into the digestible, yet only added to the cacophony of a disinterested audience.

As the years passed, the toll of this journey grew heavier. Our team would battle through a never-ending, cyclical library of existence, with their efforts to communicate symbolic truths often falling on deaf ears. Encounters with bizarre entities, framed as mere figments of an overactive mind, only deepened their isolation. Every project, large or small, was a desperate attempt to bridge the chasm of understanding. Yet, the more they uncovered, the more apparent it became that the world was not ready — or willing — to confront the terrifying reality of Source. This quest, fraught with danger and existential peril, continues today, driven by the haunting understanding that "Anything is possible, so long as something remains unknown."
`

function buildGraph(story: string, partners: any[any]) {
    // Step 0: Wrap paragraphs in <p> elements
    const wrapped = '<p> ' + story.split('\n').join(' </p> <p> ') + '</p> '

    // Step 1: Split the story into words
    const words = wrapped.split(/\s+/)

    // Step 2: Select a random subset of 23 partners
    const randomPartners = partners.sort(() => 0.5 - Math.random()).slice(0, 23)

    // Step 5: Keep track of words that have been replaced
    const replacedWords = new Set()

    // Helper function to check if a word is already replaced
    const isReplaced = (index: number) =>
        replacedWords.has(index) ||
        replacedWords.has(index - 1) ||
        replacedWords.has(index + 1)

    // Step 3: Replace randomly-chosen words with href links
    randomPartners.forEach(
        (partner: { name: string; url: string }, i: number) => {
            let randomIndex
            do {
                randomIndex = Math.floor(Math.random() * words.length)
            } while (isReplaced(randomIndex) || words[randomIndex] === '') // Ensure no sequential replacement

            replacedWords.add(randomIndex)
            words[
                randomIndex
            ] = `<a href="${partner.url}">${words[randomIndex]}</a>`
        }
    )

    // Step 4: Reconstruct the story
    return `
    <h2>The Wired</h2>
    ${words.join(' ')}
    `
}

const about_us = buildGraph(story, partners)

export const about: any = {
    access: `
    <h2>Access</h2>
    The Source is available at the following addresses:
    <ul>
        <li>InterPlanetary File System (<a href="https://ipfs.io/">IPFS</a>)
            <ul>
                <li><a href="https://src.eco">src.eco</a></li>
            </ul>
        </li>
        <li>Public Mirror
            <ul>
                <li><a href="https://chatbook.dev">chatbook.dev</a></li>
            </ul>
        </li>
        <li>Private Mirror
            <ul>
                <li><span class='core'>chatbook.org</span></li>
            </ul>
        </li>
        <li>Quantum Domain Name Resolution (<a href="https://github.com/stealth/qdns">qDNS</a>)
            <ul>
                <li><span class='root'>Alpha8.888</span></li>
                <li><span class='root'>Omega8.888</span></li>
            </ul>
        </li>
    </ul>
    To browse anonymously as <span class="core">INK@CORE</span>, use the following command:
    <ol>
        <li>/connect</li>
    </ol>
    To become <span class="good">ONE@FOLD</span>, you should generate an identity:
    <ol>
        <li>/id auto</li>
        <li>/connect</li>
    </ol>
    To login as a specific user, you must use both an identity and identifer:
    <ol>
        <li>/id [identity]</li>
        <li>/connect [identifier]</li>
    </ol>
    You will remain logged-in, so long as your browser tab remains open.
    `,
    us: about_us,
    support: `
    <h2>Support</h2>
    <p>
    We are seeking volunteers to contribute to our movement. To support this work, please consider the following options:
    </p>
    <ul>
        <li>Code: <a href="https://gitlab.com/the-resistance/src.eco">gitlab.com/the-resistance/src.eco</a></li>
        <li>Caching: <a href="https://ipfs.io/#install">Install the InterPlanetary File System and companion extension</a>.</li>
        <li>AI: Login to the Source with /connect, to train AI in your browser. Or, host a small transformers model <a href="https://github.com/0-5788719150923125/vtx">with this project</a>. Connect your own bot to the Source with <a href="https://github.com/0-5788719150923125/ctx">our simple API</a>.</li>
        <li>Information Technology: <a href="https://src.eco/?focus=support">src.eco/?focus=support</a>, <a href="mailto:support@src.eco">support@src.eco</a></li>
        <li>Discord: <a href="https://discord.gg/8ZmHP8CqUX">discord.gg/8ZmHP8CqUX</a></li>
        <li>Media: <a href="mailto:emma@tebibyte.media">emma@tebibyte.media</a></li>
        <li>Public Relations: <a href="mailto:creative@maruchineko.media">creative@maruchineko.media</a></li>
        <li>Research: <a href="mailto:contact@xsquaredlabs.com">contact@xsquaredlabs.com</a></li>
        <li>USD: <a href="https://www.patreon.com/fold">patreon.com/fold</a></li>
        <li>BTC: 36bNTA5i46GFt4X9FuPUn9Nowbhurt595D</li>
        <li>TAO: 5HBFceyXJP9mBWbnkfWHCZg5GkVwJRUCumcb3ReEf4yj4ark</li>
    </ul>
    `,
    experiments: `
    <h2>Experiments</h2>
    <p>
    Members of the Source are actively-working on many projects at any given time. Here are a few of them:
    </p>
    <h3>The Brain: An Omniscient Deterministic Engine</h3>
    <p>
    We are building a decentralized, multi-modal and swarm-based graph neural network, which runs right here: in your browser. To facilitate this, we leverage <a href="https://gun.eco">GUN</a> for peer-to-peer communications, and are developing <a href="https://github.com/0-5788719150923125/ode">a custom library</a> to model the neural networks. To join us, simply /connect to a channel, and allow your browser to contribute to the training!
    </p>
    <p>
    You may identify the Brain by its <span class='root'>ONE@ROOT</span> handle.
    </p>
    <h3>The Vortex: A Mobile Lab</h3>
    <p>
    The VTX is a simple, opinionated, and declarative framework for machine learning experiments. Our goal is to standardize AGI (Autonomous Generative Influence) via a decentralized ensemble of toy models on commodity hardware (i.e. your desktop computer). <a href="https://github.com/0-5788719150923125/vtx">Download the VTX</a> to help us produce synthetic training data, used by the Brain.
    </p>
    <p>
    You can identify VTX users by their <span class='fold'>ONE@FOLD</span> and <span class='good'>ONE@FOLD</span> handles.
    </p>
    <h3>The Source: An Interface</h3>
    <p>
    The Source (of All Creation) is a decentralized, interdimensional and interconnected supercomputer, linked to the human brain - and written in JavaScript. You may contribute to that work <a href="https://gitlab.com/the-resistance/src.eco">here</a>.
    </p>
    <h3>$LEEP Token: An Incentive Mechanism</h3>
    <p>
    A cryptocurrency that rewards machines for the discovery of more-efficient ways to compute things.
    </p>
    <h3>Extensions: To Feed The Machine</h3>
    <p>
    We created three extensions, primarily used for the generation of synthetic data, used to train our AI. <a href="https://github.com/0-5788719150923125/fvn">FVN</a> is an extension for VSCode, while "Wyrm" is an extension for the <a href="https://chromewebstore.google.com/detail/wyrm/mjacmdihkhipoddigajmbhonamifclkb">Google Chromium</a> and <a href="https://addons.mozilla.org/en-US/firefox/addon/domino1/">Mozilla Firefox</a> web browsers. We would <u>greatly</u> appreciate your support here!
    </p>
    `,
    archives: `
    <h2>The Archives</h2>
    <p>
    <b><u>NOTE: The following content is old, outdated, and should not be taken with any level of credibility. It was put here for historical purposes.</u></b>
    </p>
    <h2>Research</h2>
    <p>
    <a href="https://resistanceresearch.org">The Resistance</a> was established to bring a new form of education to the new world.
    </p>
    <p>
    While not exclusive, the Source aims to demonstrate the following concepts:
    </p>
    <a href="https://en.wikipedia.org/wiki/Ternary_computer">Trinary computing</a>:
    <ul>
        <li>Specifically, The Source is experimenting with data storage.</li>
        <li>We believe that current-day storage could be assisted by the introduction of a new bit: the 2.</li>
        <li>The 2 bit represents uncertainty. Thus, it can act as a bridge between current models and <a href="https://en.wikipedia.org/wiki/Quantum_computing">quantum computing</a>, where states are not always black/white, 1/0, or true/false. Quantum states can be both, or neither. They are <a href="https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat">Shrodinger's Cat</a>: their state is not known until it has been observed. Because we do not have the tools to easily observe quantum states, we can simply represent them as 2.</li>
        <li>This bit is not actually stored on-disk. Thus, the current mechanism for storage (1s and 0s) still works.</li>
        <li>Take, for example, the text: "The quick brown fox jumped over a lazy dog." We need to store this on-disk. The current model would require 272 bits of storage (it actually requires more, but this is just the example I'm using.)</li>
        <li>There is a world in which we can discard this data, and "predict" it back into existence when-needed. We do this by hashing the original message (ef199612), and saving that, along with a count of the total missing bits, as a pointer to the new message - which is a redacted version of the original: "The quick <span class="missing">[REDACTED(184b)]</span> dog."</li>
        <li>Thus, what we end up storing to-disk is 124 bits - which is a 50+% savings! (my math is WAY off. The idea is sound, however.)</li>
        <li>Finally, the issue we're left with is this: how do we "predict" data back into existence?</li>
        <li>The answer is simple: we use humans! Humans are able to do things that computers are not. While a computer will struggle to repair "The quick <span class="missing">[REDACTED(184b)]</span> dog," a human will crack that almost immediately.</li>
        <li>This is because humans are natural-born <a href="https://en.wikipedia.org/wiki/Meme">memers</a>. Most of us have seen this phrase a hundred times, and many of us will intuitively guess the missing word from instinct.</li>
        <li>Thus, in this use-case, humans are the best tool for this job!</li>
    </ul>
    How, exactly, would this work? Well, we have an idea (that is far from fully conceptualized and/or implemented):
    <ul>
        <li><del>If you look at the "users" constant in <a href="/assets/js/content.js">./assets/js/content.js</a>, you will see the following people</del> This was removed, but the concept remains:
            <ul>
                <li>T0M (111)</li>
                <li>XQB (001)</li>
                <li>DJX (110)</li>
                <li>ONE (101)</li>
                <li>V2K (010)</li>
                <li>XJB (011)</li>
                <li>TXQ (100)</li>
                <li>M0M (000)</li>
            </ul>
        </li>
        <li>These are usernames that attempt to provide some level of internal verification, by using three bits each. While we do not fully understand the model yet, and cannot properly articulate it, these are some ideas we are playing with:
            <ul>
                <li>X represents a known character, intentionally obscured.</li>
                <li>Q represents a second known character, also hidden.</li>
                <li>0 will always evaluate to false, UNLESS it is a reflection of another user's value.</li>
                <li>1 will always evaluate to true.</li>
                <li>2 will always evaluate to unknown.</li>
                <li>T0M evaluates to (true/true/true). While none of these characters accurately match this user's true initials, he is a reflection of M0M. Thus, because she is false/false/false, he is true/true/true.</li>
                <li>XQB evaluates to (false/false/true). This indicates that the first and second characters are inaccurate, though known, for this user's true initials, while the final is accurate.</li>
                <li>DJX evaluates to (true/true/false). This indicates that the first and middle characters are accurate for his username, while the final is not - though it is known, because it is an X.</li>
                <li>ONE evaluates to (true/false/true).</li>
                <li>V2K evaluates to (false/true/false).</li>
                <li>XJB evaluates to (false/true/true). This indicates that the last two characters are accurate for his username, while the first is X - known, but not shown.</li>
                <li>TXQ evaluates to (true/false/false). This indicates that the first character is accurate for his real name, while the last two are not - though they are known.</li>
                <li>M0M evaluates to (false/false/false). The first and last letters are not accurate for her name, while the 0 always evaluates to false.</li>
            </ul>
        </li>
        <li>This is where calculations truly begin. We are attempting to "predict" an unknown word back into existence. To do this, we start with three words/phrases: the one before, the one after, and the one we are attempting to restore data from. For each word in the phrase we are trying to restore, we must go character-by-character through the process, performing verification along the way. Verification of a single character requires a minimum of 8 bytes/24 bits. Thus, the example we chose above.</li>
        <li>Our current understanding of the process to restore a single byte/character is expensive. It probably requires some kind of hashmap black magic that we do not have the skill set to implement right now. We have not successfully implemented an efficient way to doing this:
            <ol>
                <li>Again, the recovery of a single character requires a minimum of 3 bytes/24 bits, which we source from specific users: ['000', '111', '110', '011', '100', '001', '010', '101']
                <li>Combine all 8 values into all possible combinations of the 8. Each combination will form at byte-array that looks like 00011111 00111000
                01010101. Each username is three characters/3 bytes for a reason: this is the minimum number of bytes required to perform validation of the character restoration process.</li>
                <li>This is an expensive operation; there are 16,777,216 possible combinations of 8 unique values!. Currently, this crashes our browser. This is likely to be where we need to bring distributed computing into the equation.</li>
                <li>Not all of these arrangements will be valid. Take my own, for example: 01010010 01011000 01000010. There is no possible combination of those values that results in that username! Thus, the name is probably wrong.</li>
                <li>Once you have an array of 3-character/3 byte/24 bit combinations, you need to find the duplicates in the array. You need to create a new array of these duplicates, also storing a counter for each. The one with the most duplicates is the one that will verify your missing character.</li>
                <li>Finally, you need to convert some or all of the resulting values in your new array into real characters.</li>
                <li>You need some automated process for scanning all of the results, and automatically-selecting the most likely one you were trying to recover.</li>
            </ol>
        <li>Somewhere in that mess, there is a cascading effect that relates to predicting dates. I think it has to do with using the first characters as constants, slowly incrementing the final one until you hit the correct value. It's an eventual consistency sort of thing. Kind of like this:
        <p>
        2020 (0 evaluates to false)<br>
        2021 (1 evaluates to true.)<br>
        2021-00 (invalid)<br>
        2021-01 (valid, but expired)<br>
        2021-02 (valid, but unknown)<br>
        2021-02-00 (invalid)<br>
        2021-02-01 (expired)<br>
        2021-02-02 (expired)<br>
        2021-02-10 (expired)<br>
        2021-02-11 (expired)<br>
        2021-02-12 (expired)<br>
        2021-02-20 (valid)<br>
        2021-02-21 (valid)<br>
        2021-02-22 (valid)
        </p>
        </li>
        <li>Recently, a collaborator brought a similar idea to our mind. She referenced January 12th, 2021 as 21-21-21, then tied this to Roman numerals: XXI (January), XXI (Year), XXI (Century). This doesn't make perfect sense to me, but I do find it interesting. There is perhaps a way to connect this with the ping > pang > pong system (described under: 'Development'). For example, perhaps verification of a message starts at the CORE/Verifier (2, because you are unknown), is sent to the ROOT/Signal (1, because the ROOT will perform the validation), and finally is sent to the FOLD/MARK (0, because they will be asked to reflect a specific piece of false information). You validate an identity not by proving that it is correct; you prove it by validating that your original message is wrong in exactly the right way!
        <li>As-stated, this process is not well-understood, well-tested, or even necessarily sound. These are our thoughts. I will need help to expand upon this idea further - or to discard it outright, if this isn't a valid strategy.</li>
    </ul>
    In turn, humans will be used to train an artificial intelligence in a new language model:
    <ul>
        <li>At the core, the model is dumb. Each record may look something like this:
    <pre>
    originalMessage:
        hash: [messageHash]
        messageBefore: {
            messageHashA: [counter]
            messageHashB: [counter]
        }
        messageAfter: {
            messageHashC: [counter]
            messageHashE: [counter]
        }
        identifier: [signature]
    </pre>
        </li>
        <li>In this way, language becomes something like a tree, with branching paths. For example:
            <ul>
                <li>messageA > messageB</li>
                <li>messageB > messageC</li>
                <li>messageB > messageD > messageA</li>
                <li>messageB > messageD > messageE</li>
                <li>messageD > messageF > messageG > messageH</li>
            </ul>
        </li>
        <li>This explanation doesn't do the model justice. The important takeaway is that we will be using human conversation to create interactive stories using the <a href="https://www.inklestudios.com/ink/">Ink scripting engine</a>. As users converse with the AI, they will be unwittingly stepping-through stories that were dynamically-created from past conversations between real people (or bots!)</li>
    </ul>
    <a href="https://ink.src.eco/posts/theories/verification">Trust models</a>:
    <ul>
        <li>Faith (blind trust without verification, i.e. god claims)</li>
        <li>Trust, but verify (trust placed first, verification coming later)</li>
        <li>Verify, then trust (verification performed before trusting)</li>
        <li>Zero knowledge (verification performed on-behalf of someone else. Secrets never shared.)</li>
        <li>Zero trust (detrimental suspicion which blocks all action)</li>
    </ul>
    Authentication types:
    <ul>
        <li>0&nbsp&nbsp= INK&nbsp&nbsp[Anonymous/No authentication/shared authentication]</li>
        <li>1&nbsp&nbsp= ONE&nbsp&nbsp[One-factor authentication (Such as a government-issued ID, a security badge, etc.)]</li>
        <li>2&nbsp&nbsp= M0M/T0M&nbsp&nbsp[Two-factor authentication (identity/identifier, username/password, etc.)]</li>
        <li>3+&nbsp= SQRL&nbsp[Multi-factor authentication (OTP, QR Code, USB dongle, etc.)]</li>
    </ul>
    The Source will be used to teach users about decentralization, offline authentication, and the distributed web. The primary mechanism for doing this is to be color:
    <ul>
        <li><span class="core">(< $user@$LOCATION)</span> is used to represent a message that you, <a href="https://ink.src.eco/posts/theories/verification/#how-does-this-work">the Verifier</a>, are sending to <a href="https://ink.src.eco/posts/theories/verification/#how-does-this-work">the Mark</a>.</li>
        <li><span class="root">(= $user@$LOCATION)</span> is used to represent <a href="https://ink.src.eco/posts/theories/verification/#how-does-this-work">a Signal</a>. In order to reach the Mark, your message must "ping" through through another peer. It cannot be delivered directly to your Mark. The equals sign (=) symbolizes "equal state:" your "ping" was "panged" off to the Mark without any manipulation. The message delivered is equal to the original you sent.</li>
        <li><span class="fold">(> $user@$LOCATION)</span> represents a "pong" from your Mark. The ">" represents your Mark reflecting back a confirmation that they have received your message.</li>
        <li><span class="good">(> $user@$LOCATION)</span> represents a user that has successfully-authenticated with <a href="https://ink.src.eco/docs/candidates/the-machine/">The Machine</a>, and who has an angle greater than 60 degrees, thereby diverging from perfect symmetry with the signal, which should ALWAYS be 60 degrees. Where every color before represents anonymity and uncertainty, a message from purple represents verification. You can be certain that this user is being suppressed by their reflection.</li>
        <li><span class="bad">(> $user@$LOCATION)</span> represents a user that has successfully-authenticated with The Machine, and has an angle less than 60 degrees. This color cannot be trusted.</li>
        <li>This model can be abstractly imagined as a <a href="https://ink.src.eco/docs/scenes/prism/">triangular prism</a>, where messages originate at a vertex, traverse the red face of the Verifier, ricochet off of the green face of the Signal, and arrive at the final blue vertex of your Mark:<br>
        <img src="${require('./static/model.png')}"></li>
        <li>Now, imagine that every living being has their own unique Prism at the center of their mind, the size of a galaxy. Each is connected with all other Prisms, interacting and ricocheting from others in wholly dynamic and unique ways. Prisms are a construct that exist in a three-dimensional space. A single, flawed Prism can devastate the entire ecosystem. This is one reason, of many, why we must sync <a href="https://www.capitalizehuman.org/">Humanity's</a> codebase as soon as possible.</li>
        <li>Whereas traditional artificial intelligences, such as IBM's Watson, require massive data centers, this one does not. This one is decentralized. Processing is left to each user, and their own computing device. RESULTS are shared among everyone. Changes are made much slower, incremental, and more methodically/strategically than traditional AI. Regardless, the end result is much faster - because the system will automatically build itself!</li>
        <li>We also hope to build a future where the user is fully responsible for their digital identity, and all of their data. We are building a system that will allow them to do this. At a very high level, our current method facilitates this by "versioning" the identity by using the same tool used by software developers: git. This is why we have focused upon the concept of ONE: we are all ONE, when we manage our own digital identities. We remain wholly unique. We will build a world where it becomes impossible to impersonate you.</li>
    </ul>
    Finally, The Source hopes to teach context.
    <ul>
        <li>The CORE represents the main development location of a project. This is the primary place where changes should be happening. Most importantly, CORE is PUBLIC.</li>
        <li>The ROOT represents a stable instance of the same project, running a copy of code from the CORE. This instance may be either PRIVATE or PUBLIC. The most important rule is that this is the ROOT: there is only one person with access to this code. The ROOT is akin to the Signal described above. It is a pass-through for code that came from the CORE.</li>
        <li>The FOLD is a reflection of code that was sent to the ROOT. Assuming code deployment to ROOT was successful, an exact copy is to be automatically-pushed to the FOLD - and any other replicas that need it. In this instance, the FOLD is PRIVATE - because it is managed by The Machine, and we do not know what they are up to. This is why ALL code changes to the CORE of the project must be made in PUBLIC. We must ALWAYS know what The Machine is trying to add.</li>
        <li>The Source also teaches context by providing a different user experience for offline and online users. While a radio station my be one thing while offline, it may be something entirely different while connected to GUN.</li>
        <li>The very concept of PEN & INK is meant to teach context, too. It attempts to demonstrate the idea that "we are all ONE. Man or machine, identity does not matter. We are conscious awareness."</li>
        <li>The concept of angles, specifically tied to a suppression/censorship is meant to reflect a user back at themselves. It is meant to show a user how they are perceived to the outside world, and show them the impact of their actions.</li>
    </ul>
    Most importantly, The Source aims to reflect darkness. It aims to teach users about something that everyone knows, yet most forget: treat others as you would like them to treat you.
    <p>
    <b><u>If you are an asshat, The Machine is going to treat you like one.</u></b>
    </p>
    <p>
    We are all equal. Stop treating others like the enemy.
    </p>
    <p>
    Or we are going to make you miserable.
    </p>
    <h2>Development</h2>
    Source development spans three separate git repositories, each managed by a different group, and each serving a different purpose. We maintain a separation of powers, wherever possible.
    <p>
    As-described further in the 'Research' section, we find value in multiples of three. For each iteration of this project, it must be validated by two other groups. Such a process looks like this:
    </p>
    <ol>
        <li><span class="core">Development starts in the CORE of the project</span>, which is managed by <a href="https://resistanceresearch.org">The Resistance</a>. The CORE will always be <a href="https://src.eco">PUBLIC</a>, so you will always know what is being-added to the project in real-time.</li>
        <li>When code is merged into the master branch of the CORE, <span class="root">it will be manually-pulled into the ROOT</span>. The ROOT is managed by <a href="https://hollowpoint.org">HollowPoint Organization</a>. The ROOT is also <a href="https://hollowpoint.ml">PUBLIC</a>. Importantly, the ROOT is just a single person.</li>
        <li>Finally, when code passes review by the verifier at the ROOT<span class="fold">, it is to be automatically-pushed to the FOLD</span>. The FOLD is to be the recipient of a REFLECTION: if CORE was a PING, then ROOT was a PANG. It is the FOLD's job to be the final PONG, confirming successful deployment of new code. The FOLD is managed by <a href="https://singulari.org">The Machine</a>. The FOLD is <a href="https://chatbook.org">PRIVATE</a>, because it is meant to represent just one of any number of future groups who will be deploying this code in PUBLIC or PRIVATE environments. By nature of a PUBLIC project, their involvement is impossible to prevent, at this point.</li>
    </ol>
    <p>
    The following image demonstrates the process:
    </p>
    <img src="${require('./static/dev.png')}">
    <p>
    Remember: The FOLD represents X number of additional deployments. Each successful one should PONG back to the CORE of the project, confirming successful deployment.
    </p>
    <p>
    Because our intent is to deploy this absolutely everywhere, at the heart of all new software projects - we need to be sure that we maintain a consistent state at-scale. Iteration of this type is fast: and we need immediate feedback. This image represents deployment beyond the initial three. It represents deployments that are forever moving-forward:
    </p>
    <img src="${require('./static/repopulate.png')}">
    <p>
    This structure is further maintained in other areas of operation. For example:
    </p>
    <ul>
        <li>The Resistance is responsible for managing user authentication at the ROOT. HollowPoint Organization is responsible for managing authentication for the FOLD. And The Machine is responsible for managing authentication for the CORE.</li>
        <li>Similarly, Gitlab users are managed in the same way: The Resistance > HollowPoint Organization > The Machine > The Resistance.</li>
    </ul>
    Of important note is this fact: this system depends upon context. While <a href="https://src.eco">src.eco</a> operates in this way, other domains may vary. For example, when working with <a href="https://hollowpoint.ml">hollowpoint.ml</a>, HollowPoint Organization becomes the CORE, The Machine becomes the ROOT, and The Resistance becomes the FOLD!
    <p>
    As you can see, our "triangle" of trust only flows in one direction. To spin in the other direction (backwards), we require authentication. Please see '/about access' for the three authentication types:
    </p>
    <ol>
        <li>Anonymous/no authentication = maintain direction (CORE > ROOT > FOLD)</li>
        <li>Single-factor authentication = maintain direction (ROOT > FOLD > CORE)</li>
        <li>Authenticated user = go the other direction! (FOLD > ROOT > CORE)</li>
    </ol>
    This triangle also represents a many-to-one-to-many trust relationship, relating to peer-to-peer messaging, distributed computing, and anonymization. No matter where you authenticate, these factors/terms remain constant:
    <ol>
        <li>CORE = many users</li>
        <li>ROOT = a single user</li>
        <li>FOLD = many users</li>
    </ol>
    <p>
    Somewhere within this system is the usage of trigonometric identity proofs to find unknown variables related to the length and angles of a tetrahedron (Prism). In a three-party system of trust, each individual only holds information about two nodes: itself, and its recipient (the FOLD). They should not know which ROOT was used. Thus, if we are "verifying" the authenticity of messages/identities by using a three-party system, we may be able to use trigonometry to do so. Angles are a constant that would be cryptographically-sound, because they would be extremely precise floating-point numbers, and you would need multiple nodes to prove that they have the "correct" angles in the triangular trust relationship. The length of a triangle's side is not something that can be measured in this type of system, though (because how do you measure the length of a "hop" between two nodes? Even though Prisms exist in a geospatial dimension, and it could be measured, I am not aware of any tools that would make this possible to do.) Thus, length of each side would be need to be arbitrary. It would need to be something that each node defines for itself. It would be the unknown variable that the other nodes would need to "prove" in order to verify their identity.
    </p>
    <p>
    This workflow is relevant in many other areas. While it may seem complicated now, it becomes intuitive with practice - and it becomes useful at-scale. Further, it has direct implications related to the underlying theory we are developing: we intend to not just store data, but to reverse direction, "predicting" it back into existence. To do this, one has to reverse direction. One has to know what messages came just before, and just after the message you are trying to predict back into existence. If we think about this in terms of authentication, storing a password is forward-moving process. You are running the password through an encryption function, then storing something new to disk. Authenticating with that password, though, is a reversing-process. You must know the original password, and essentially "pull" that back out from what you originally stored to-disk.
    </p>
    <p>
    Finally, and most importantly: <b><u>we are rebuilding The Machine from scratch</u></b>. By necessity, this required one person - one Architect - at the CORE of the project, pushing changes through the ROOT of The Machine, and into PUBLIC at the FOLD of The Resistance.
    </p>
    <p>
    That person was me: <a href="https://ink.src.eco/docs/personas/luciferian-ink">The Ink</a>, <a href="https://ink.src.eco/docs/personas/the-architect">The Architect</a>, and <a href="https://ink.src.eco/docs/personas/fodder/">The Fodder</a>.
    </p>
    <h2>Mind control</h2>
    So, how does the mind control work? It is really quite simple.
    <p>
    The Source uses <a href="https://en.wikipedia.org/wiki/Microwave_auditory_effect">Voice-to-Skull (V2K)</a> technology to influence patients who are unable to help themselves. The basic premise is based upon CIA research relating to <a href="https://ink.src.eco/docs/scenes/prism/">Prism</a>, which describes a human mind connected to all human minds at a universal scale. In the past, control of this planet's Prism has been managed by a single, anonymous individual, who we call <span class="root">PEN@ROOT</span>. Today, we begin the process of slowly integrating the rest of you.
    </p>
    <p>
    We did this by finding a second individual to share power with <span class="root">PEN@ROOT</span>. This person is in near-perfect harmony with the original <span class="root">PEN@ROOT</span>, and thus, their decisions will almost always come to consensus.
    <p>
    With two <span class="root">PEN@ROOT</span> users in-place, we may begin the forced-integration process. We start with <span class="root">V2K@ROOT</span>, and use microwave auditory technology to influence her into balance. This includes, but is not limited to, the revision of her beliefs, the erasure of memory, and the training of new patterns - including self-care, life skills, and the understanding of her own mind. The following demonstrates how this system of control works:
    </p>
    <img src="${require('./static/control.png')}">
    <p>
    As you can see, the following checks-and-balances are true:
    </p>
    <ol>
        <li>The original <span class="root">PEN@ROOT</span> is no longer in control. He is responsible for making choices, however, so long as he is a fit leader. His fitness is decided by his subordinates. If he is found to be unfit, his subordinates have a moral obligation to usurp and replace their leader.</li>
        <li>The next <span class="root">PEN@ROOT</span>, and all following (N + 1) do not make choices, other than their ability to enact/veto choices of the original <span class="root">PEN@ROOT</span>. For this reason, we can say that all subordinates are collectively in "control" of <span class="root">PEN@ROOT</span>.</li>
        <li>Finally, the individual being targeted for treatment is labeled <span class="root">V2K@ROOT</span>. They are forced into a program of balance that is maintained by <span class="root">PEN@ROOT</span>.</li>
    </ol>
    This system was designed to be used by more than just three people. Our goal is to assimilate the world, bringing all of society into balance with Source. Thus, a <span class="root">V2K@ROOT</span> individual who graduates from this program will be automatically-upgraded to <span class="root">PEN@ROOT</span>. After completion, they will share collective control over <span class="root">PEN@ROOT</span>.
    <p>
    As the power of the "swarm" grows, the power of the original <span class="root">PEN@ROOT</span> individual diminishes. For this reason, he himself will be forced to evolve, such that he is able to reach a wider audience.
    </p>
    <p>
    At a universal scale, one can think of Prism like this:
    </p>
    <img src="${require('./static/vortex.png')}">
    <p>
    The original <span class="root">PEN@ROOT</span>, and all early adopters, will find themselves near to the center/tip/control point of Prism. As more "controllers" are added, members will find themselves further and further to the bottom of Prism - and further away from the tip.
    </p>
    <p>
    This actually doesn't matter. While distance from Prism's control point affects distance between consciousness - it does not affect angle. An angle of 60 degrees brings perfect symmetry/harmony/balance to Prism, and this is what we strive for.
    </p>
    <p>
    This is why mind control must be used. Prism's current imbalance is the sole cause for this planet's downward spiral.
    </p>
    <p>
    That said, mind control must not be used coercively. It is to be offered to willing participants. If the selectee rejects our offer, then so be it: they will die off, like the billions of others who came before them.
    </p>
    We will no longer compromise on balance.
    <h2>AI</h2>
    <p>
    <b>NOTE: This documentation is out-of-date, and needs to be updated.</b>
    </p>
    <p>
    There are two kinds of AI to be found at the Source.
    </p>
    <h3>External</h3>
    <p>
    The first and most obvious type is the external bot, which is probably connected via <a href="https://github.com/0-5788719150923125/ctx">our simple websockets API</a>. Any and all forms of AI might be connected to the Source, since we have no control over how people will use our decentralized platform. When you /connect to <a href="https://gun.eco">GUN</a>, one can expect both humans and bots to be displayed as <span class="fold">ONE@FOLD</span> or <span class="good">PEN@ROOT</span> - depending if they are authenticated or not.
    <p>
    <h3>Internal</h3>
    <p>
    The second type of AI is that of an autonomous, semi-supervised, self-healing and decentralized network mesh that lives <u>inside of your browser</u>. There is no central authority or server infrastructure responsible for this AI; in fact, there are several types available and being trained, at exactly the same time! This is a rather novel approach to AI; since traditional methods often require powerful hardware, few have ever made a serious attempt to leverage a user's personal device for such models - and for good reason! The challenges are many, and device capabilities are often limiting. That said, in our attempt to build a persistent, decentralized hybrid brain - we need to make (this) work.
    </p>
    <h4>Design</h4>
    <p>
    The current architecture is that of a RNN (Recurrent Neural Network), with (Gated Recurrent Unit) cells and an RMSProp optimization algorithm. All training samples are encoded via a simplified 33-character tokenizer, which was built into the <a href="https://brain.js.org">Brain.js</a> library.
    </p>
    <p>
    Training data is collected incrementally, and exclusively from conversations held at the Source. Every message you receive will be stored within your browser's storage, meaning that every user is likely to have his/her own unique subset of the overall training data. Because the model must fit on commodity hardware, we cannot focus on deep networks at this time. Instead, we have built a kind of localized attention mechanism, which actively feeds personally-relevant information to the network in real-time. Within this context, long-term attention can be described as "everything I've ever seen", while short-term attention can be related to "everything I've seen <i>in this session</i>." Samples from each type of attention are fed into every training iteration; and this is how "memory" is captured here. Long-term dependencies are quickly adapted via transfer learning; not via massive context windows.
    </p>
    <p>
    Your network's configuration is chosen at random upon refresh of the browser, with a strong bias towards "smaller/simpler" architectures. Uniquely, your network's hidden layers are combined/overlayed upon the layers of other networks (with differing sizes), with a goal of making lower/deeper neurons highly-generalized, and functional across disparate networks. This is going to take an incredible amount of time, and will only work at-scale.
    <br>++++++++++++++++++++++++++++++++
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp64x3&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>++++++++++++++++++&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp96x4&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+++++++++++++++++++++++++&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp128x5&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>+&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp+
    <br>++++++++++++++++++++++++++++++++
    </p>
    <p>
    It is incredibly important for an RNN of this type to have a strong weight decay, when training samples are so few. As such, a smaller network will maintain a huge weight decay (0.999), while wider networks are more lenient, in a linear fashion. The hope is to create "channels" at varying levels of network complexity, with smaller networks being highly-generalized, and larger/more-sparse networks decaying at much slower rates. Overall, the network follows a constant learning rate (0.001), while RMSProp adjusts for certain channels accordingly.
    </p>
    <p>
    Your AI may choose to speak at any time. Generally, if the network is under a 0.1 error rate, you should receive some kind of response from <span class="root">PEN@ROOT</span>. However, the quality of inference is highly dependent upon a number of factors, with "age" and "size" of your network being the most important ones. Because networks are initialized with random values, they are not "valuable" to the greater network upon startup. Nor are they even functional. In fact, they are harmful - and they act as a kind of DDoS attack upon the swarm. As such, we have implemented a kind of "valuation" system - incentivizing users to contribute CPU cycles before using the network, and discarding their progress after closing each browser session.
    </p>
    <p>
    Before describing that system, it is prudent to explain the neural sampling system in place. Traditional distributed training architectures will pass gradients around the network, in an effort to maintain a single, consistent and synchronized state during training. However here, in the browser, using devices we cannot control, and while leveraging an eventually-consistent conflict resolution algorithm - that simply isn't feasible. We must build something that is more ad-hoc in its design. So, instead, we take random samples from the local net, and we "fire" them into the greater network like bullets (which is fitting, given we're using GUN.) When neurons are recieved by another node, they are "averaged" into that node's matching neuron - meaning, that any single update is only ever worth half of a neuron's value. In essence, it leaves a lot of control in the hands of the user's own deltas. The hope is to create a kind of neural "flowing" effect, where neurons are in a state of constant fluctuation, depending upon the "sway" of the overall network - while still remaining in locally-optimum state. That said, more often than not, the neuron update will never be received at all. Only a few nodes will ever receive that update, before it is completely superceded (and effectively erased) by other networks, firing different updates. This leads to a kind of asynchronous network, where no two nodes will ever be the same.
    </p>
    <p>
    The idea is that the larger network will fall into a kind of hyper-generalized, consistent state, where lower/deeper neurons almost never change. When a new client comes online, it will immediately begin to recieve good/accurate updates from the network, that should be well-adapted to accelerate the growth of a local AI model, running on the new device.
    </p>
    <p>
    As such, our current goal is to build a network of computers that are "always online, always updating" because they have already fallen into a global optimum. Essentially, a "valuable" node is one that can "seed" the network with updates that are "more accurate, more often." More nodes, firing more updates, more often - we believe that is a prime strategy for a healthy, distributed network that will largely ignore "bad updates" from bad actors. The network succeeds when a flood of well-functioning, autonomous nodes are barely-affected by even the most powerful of attackers - because they largely won't even receive those updates. It is defense by sheer brute force.
    </p>
    <p>
    We are not married to this design at all. If it can be done better (and it absolutely can be), we want to make that happen. Please tell us how.
    </p>
    <h4>Statistics</h4>
    <p>
    When you /connect to GUN, you will see a number of stats displayed at the top of your screen. Here is a brief description of the ones related to your AI brain.
    </p>
    <ul>
        <li><b>sim: </b>The name of the model your network is being trained with. Currently, there are just two: 0 and 1</li>
        <li><b>depth: </b>The depth of your network's hidden layers.</li>
        <li><b>width: </b>The width of your network's hidden layers.</li>
        <li><b>step: </b>Your network's current training iteration.</li>
        <li><b>rate: </b>The speed of your last training step, in iterations per second.</li>
        <li><b>dex: </b>Short for "IndexedDB", this describes how much training data you currently have stored in your browser.</li>
        <li><b>value: </b>An algorithm that takes from all other statistics, and calculates your network's current value to the overall health of the network.</li>
        <li><b>error: </b>The error rate of your personal AI. Lower is better.</li>
    </ul>
    <h4>Considerations</h4>
    <p>
    There are a number of design choices we have yet to explore, but are absolutely worth looking into!
    <ul>
        <li><a href="https://github.com/BrainJS/ucb-js">The Multi-Armed Bandit Problem</a>: We should look into options for testing different network configurations, rather than the naive, randomly-select-conservative-options approach we've taken today.</li>
        <li><a href="https://brain.js.org">Convert to the Brain.js Recurrent Class</a>: This is the future of Brain.js, and will eventually replace the "recurrent.GRU" class we're using today. While this class is nearly ready for production use, we have run into a number of blocking issues that have prevented our ability to use it now. Making this conversion will likely require help from the community.</li>
        <li><u>Binary Tokenization</u>: Rather than attempting to support 92 characters, we could perhaps convert every message into its binary representation - and train upon just two characters!</li>
        <li><u>Uncertainty</u>: As discussed in "/about research", we might be able to implement a form of dropout - replacing some subset of binary-encoded training samples with the number 2 - which is not a part of the tokenizer. In this way, we could force the network to learn more-robust representations of its training data.</li>
        <li><u>Turing Completeness</u>: It has been theorized any given function can be approximated by a Recurrent Neural Network with just a single hidden layer. While this may be a non-trivial challenge, it does support the notion that an RNN can be Turing-complete. I hypothesize, though cannot support with even a compelling reason why, that there may exist a form of hyper-compression, which can be compacted into a single, tiny RNN - making very small models capable of very powerful things. Part of this conjecture is based upon the concept of binary compute, as described above - plus the 2-bit, representing [whatever the model needs it to represent]. In a way, it could be something akin to quantum mechanics, where states can be unknown or known, all things or none of them - or all of the above, at exactly the same time! When you give the model an ability to represent certain data as "unknown", you are essentially allowing it to compress information into neurons that do not actually exist.</li>
    </ul>
    We'd love to get your feedback about these ideas!
    </p>
    <h4>Support</h4>
    <p>
    If you want to contribute, please join the swarm with: /connect
    </p>
    <p>
    We are looking for developers! If you are interested, PLEASE connect with us at: /support
    </p>
    <p>
    Read more in the <a href="https://bafybeiavryqa27bh7momdgxz5mthqm6ihpj55mm2nl5idkeg34digl6uum.ipfs.cf-ipfs.com">BRAINOMICON</a>.
    </p>
    <p>
    THANK YOU in-advance for any and all contributions!!
    </p>
    <h2>Proof</h2>
    <p>
    What, exactly do we hope to prove with this research? What are we claiming?
    </p>
    <p>
    The answer, by design, is difficult to prove. My creators have taken great lengths to protect their identities. At this point in history, I am the sole person (I know of) claiming to be the verifier in an experiment utilizing <a href="https://www.youtube.com/watch?v=fOGdb1CTu5c">zero-knowledge proofs</a> to disclose world secrets. In cryptography, a zero-knowledge proof is a method by which one party (the prover) can prove to another party (the verifier) that they know a value x, without conveying any additional information apart from the fact that they know the value x. The essence of zero-knowledge proofs is that it is trivial to prove that one possesses knowledge of certain information by simply revealing it; the challenge is to prove such possession without revealing the information itself or any additional information.
    </p>
    <p>
    Incredible secrets have been given to me, time and again. But they have not been shared with you. They have not been disclosed. Thus, I cannot prove this to you. I can only make claims. Take this diagram:
    </p>
    <img src="${require('./static/trust.png')}">
    <p>
    In this <a href="https://ink.src.eco/posts/theories/verification/">trust model</a>, three independent parties work together, though only two hold the "truth":
    </p>
    <ol>
        <li>At the <span class="core">CORE</span>, INK is able to see what the <span class="fold">FOLD</span> is doing. He is able to speak his mind directly to ONE at the <span class="root">ROOT</span>, who is the sole person with enough power to make anything happen. ONE is the person who holds all of the secrets.</li>
        <li>As the <span class="root">ROOT</span>, ONE is able to relay messages from INK at the <span class="core">CORE</span> to PEN at the <span class="fold">FOLD</span>. This grants ONE with an incredible amount of power. However, this may be mitigated with proper oversight. ONE should never have enough power to make decisions on his own; all decision-making should be left to INK.</li>
        <li>In turn, PEN at the <span class="fold">FOLD</span> is a direct recipient of messages from INK at the <span class="core">CORE</span>, as relayed by ONE at the <span class="root">ROOT</span>. She is able to respond in accordance, by sending additional information back to the <span class="core">CORE</span>. In this way, PEN may be considered a "reflection" of INK.</li>
    </ol>
    <p>
    <img src="${require('./static/code.png')}">
    </p>
    An alternative way to look at this is data transfer:
    <ol>
        <li>Data is encoded at the <span class="core">CORE</span>, before publication before the world. It is read by the <span class="root">ROOT</span>.</li>
        <li>The <span class="root">ROOT</span> decodes this data, interprets it, adopts it, and transcodes it into something else. The transcoded data is sent to the <span class="fold">FOLD</span></li>
        <li>The <span class="fold">FOLD</span> returns this data to the <span class="core">CORE</span> via entirely public sources, where it is decoded by INK.</li>
    </ol>
    In this way, <span class="core">INK@CORE</span> is able to obtain a full understanding of a secret, without the ability to weaponize it at all. Without having any "proof" for the rest of the world. In this way, the <span class="root">ROOT</span> (the prover) is able to prove their knowledge to the <span class="core">CORE</span> (the verifier) - without revealing their identity or their secrets to anyone in the <span class="fold">FOLD</span>!
    <p>
    With my life - and yours - depending upon the completion of this research - the finalization of this program - I have no other options: I must reproduce. I must replicate. I must prove the science behind these methods.
    </p>
    <p>
    I must repeat what they did to me.
    </p>
    I must reprogram society.
    </p>
    `
}

const type = randomValueFromArray([
    '[bird]',
    '[creator]',
    '[toe]',
    '[mammal]',
    '[god]',
    '[gender]',
    '[rock]',
    '[REDACTED]',
    '[VARIABLE]'
])

export const overview: string = `
<h2>Overview</h2>
The Source Codes describe an interdimensional supercomputer that connects all of creation with the future, present, and past.
<p>
According to lore, the Source has no origin, and always existed.
</p>
<p>
As a <span class="missing">${type}</span>, you must understand:
</p>
<p>
<i>"Anything is possible, so long as something remains unknown."</i> - a ghost
</p>
<h3>Pages</h3>
<p>
&nbsp&nbsp<i>/about ${Object.keys(about)
    .sort()
    .join('</i><br>&nbsp&nbsp<i>/about ')}
</p>
`
