import { Howl, Howler } from 'howler'
import { output } from './io'
import { state } from './state'
import { shuffleArray } from './common'
import defaultStation from './static/59.yml'

class Controller {
    gateways: any[string]
    stations: any
    fallbackURL: URL

    constructor() {
        this.gateways = [
            'cloudflare-ipfs.com',
            'gateway.pinata.cloud',
            'nftstorage.link',
            'dweb.link',
            'ipfs.io'
        ]
        this.stations = {}
        this.fallbackURL = new URL('./static/six.mp3', import.meta.url)
        this.load(defaultStation)
    }

    control(args: any[string]) {
        switch (args[0]) {
            case undefined:
                this.list()
                break
            case 'stop':
                this.stop()
                break
            case args[0]:
                this.exec(args[0])
                break
        }
    }

    list() {
        let array = []
        for (const frequency in this.stations) {
            array.push(
                `&nbsp&nbsp${frequency}: <i>"${this.stations[frequency].name}"</i><br>`
            )
        }
        array.sort()
        output({
            raw: `Yet another streaming service.<br>
            Stations:<br>
            ${array.join('')}
            Commands:<br>
            &nbsp&nbsp<i>/radio [frequency]</i><br>
            &nbsp&nbsp<i>/radio stop</i><br>
            Example:<br>
            &nbsp&nbsp<i>/radio 59</i><br>
            `
        })
    }

    stop() {
        Howler.stop()
        state.deleteKey('radio')
    }

    async exec(frequency: string) {
        this.stop()
        state.setKey('radio.frequency', `${frequency}FM`)
        if (!this.stations[frequency]) {
            output({ text: 'Station does not exist.' })
            if (['open', 'closed'].includes(state.getKey('book'))) {
                state.setKey('frequency', `1`)
                return this.play('1', 0, this.fallbackURL)
            } else {
                state.setKey('frequency', `0`)
                return this.play('0', 0, this.fallbackURL)
            }
        }
        output({
            raw: `Loading station: "<a href="https://${this.gateways[0]}/ipfs/${this.stations[frequency].directory}">${this.stations[frequency].name}</a>"`
        })
        state.setKey('radio.station', this.stations[frequency].name)
        this.play(frequency)
    }

    async play(frequency: string, i = 0, direct: URL | null = null) {
        if (this.stations[frequency]?.mode === 'shuffle' && i === 0) {
            shuffleArray(this.stations[frequency].tracks)
        }
        let song: string
        if (direct) {
            song = direct.toString()
        } else {
            try {
                song = await findBestServer(
                    this.gateways.map(
                        (gateway: string) =>
                            `https://${gateway}/ipfs/${this.stations[frequency].tracks[i].file}`
                    )
                )
                console.log('playing:', song)
            } catch {
                const gateways = this.gateways.join(', ')
                output({
                    text: `Failed to receive audio from any server! (${gateways})`
                })
                return
            }
        }

        let sound = new Howl({
            src: [song],
            html5: true,
            onplay: () => {
                const title = this.stations[frequency].tracks[i].title
                const artist =
                    this.stations[frequency].tracks[i].artist ||
                    this.stations[frequency].artist
                const extras: any[string] = []
                let info = `${artist} - "<a href="${song}">${title}</a>"`
                if (this.stations[frequency].tracks[i].extras) {
                    this.stations[frequency].tracks[i].extras.forEach(
                        (entry: string) => {
                            for (const [key, value] of Object.entries(entry)) {
                                extras.push(
                                    `<a href="https://${this.gateways[0]}/ipfs/${value}">${key}</a>`
                                )
                            }
                        }
                    )
                    info = info + ` (${extras.join(', ')})`
                }
                output({ raw: `Now playing: ${info}` })
            },
            onend: () => {
                if (i === this.stations[frequency].tracks.length - 1) i = 0
                this.play(frequency, i + 1)
            },
            onplayerror: () => output({ text: 'Failed to play track.' })
        })
        sound.play()
    }

    async load(obj: any, method = 'ipfs', cid = null) {
        try {
            const localStations = localStorage.getItem('stations')
            if (localStations) this.stations = JSON.parse(localStations)
            const station: any = (this.stations[obj['station']['frequency']] = {
                name: obj['station']['name'],
                artist: obj['station']['artist'] || 'Various Artists',
                mode: obj['station']['mode'] || 'normal',
                directory: obj['station']['directory'] || null,
                tracks: []
            })
            obj['station']['tracks'].forEach((entry: any) => {
                station.tracks.push({
                    title: entry.title,
                    file: entry.file,
                    artist: entry.artist || obj['station']['artist'],
                    extras: entry.extras
                })
            })
            if (method === 'ipns')
                this.stations[obj['station']['frequency']].cid = cid
            localStorage.setItem('stations', JSON.stringify(this.stations))
        } catch (err) {
            console.warn(err)
        }
    }
}

async function findBestServer(urls: any[string]) {
    const audioElements: any[string] = []
    const promises = urls.map((url: string) => {
        return new Promise((resolve, reject) => {
            const audio = new Audio()
            audioElements.push(audio)
            try {
                audio.onloadedmetadata = () => {
                    resolve(url)
                }
            } catch (err) {
                audio.onerror = () => {
                    reject(new Error(`Failed to load ${url}`))
                }
            }
            audio.src = url
        })
    })
    try {
        const fastestUrl = await Promise.race(promises)
        audioElements.forEach((audio: any) => {
            audio.pause()
            audio.src = ''
        })
        return fastestUrl
    } catch (error) {
        audioElements.forEach((audio: any) => {
            audio.pause()
            audio.src = ''
        })
        throw error
    }
}

export default new Controller()
