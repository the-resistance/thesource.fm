import * as Tone from 'tone'
import { output } from 'src/io.js'
import { delay } from './common'

class Controller {
    async control(args: any[string]) {
        output({
            raw: `<br><img src="${require('url:./static/CORE.gif')}">`,
            textClass: 'terminal'
        })
        await delay(6000)
        // Example Tone.js procedural generation implementation: https://jsfiddle.net/qcsxnweh/3
        output({ text: 'ERROR: Method not implemented.' })
        await delay(2000)
        playSong()
    }
}

async function playSong() {
    const message = 'the quick brown fox jumped over a lazy dog'

    // const sampler = new Tone.Sampler({
    //     urls: {
    //         C4: 'C4.mp3',
    //         'D#4': 'Ds4.mp3',
    //         'F#4': 'Fs4.mp3',
    //         A4: 'A4.mp3'
    //     },
    //     release: 1,
    //     baseUrl: 'https://tonejs.github.io/audio/salamander/'
    // }).toDestination()

    const synth = new Tone.Synth().toDestination()
    let now = Tone.now()
    const unit = 0.16
    message.split('').map((char: string) => {
        const code = convertToMorse(char)
        if (char === ' ') {
            // The space between words is 7 time units.
            now = now + unit * 7
        } else {
            // The space between letters is 3 time units.
            now = now + unit
        }
        let lastSymbol = ''
        code.split('').map((symbol: string) => {
            // The space between symbols (dots and dashes) of the same letter is 1 time unit.
            if (lastSymbol !== symbol) {
                now = now + unit
            }
            // The length of a dot is 1 time unit.
            if (symbol === '.') {
                now = now + unit
                // A dash is 3 time units.
            } else if (symbol === '-') {
                now = now + unit * 3
            }
            synth.triggerAttackRelease('A4', '16n', now)
            lastSymbol = symbol
        })
    })
}

function convertToMorse(str: string) {
    return str
        .toUpperCase()
        .split('')
        .map((el) => {
            // @ts-expect-error
            return morseCode[el] ? morseCode[el] : el
        })
        .join('')
}

const morseCode = {
    A: '.-',
    B: '-...',
    C: '-.-.',
    D: '-..',
    E: '.',
    F: '..-.',
    G: '--.',
    H: '....',
    I: '..',
    J: '.---',
    K: '-.-',
    L: '.-..',
    M: '--',
    N: '-.',
    O: '---',
    P: '.--.',
    Q: '--.-',
    R: '.-.',
    S: '...',
    T: '-',
    U: '..-',
    W: '.--',
    X: '-..-',
    Y: '-.--',
    Z: '--..',
    ' ': ' '
}

export default new Controller()
