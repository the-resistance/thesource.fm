interface Station {
    name: string
    frequency: string
    artist: string
    mode: string
    directory: string
    tracks: any[any]
}

declare module '*.yml' {
    const content: Station
    export default content
}
